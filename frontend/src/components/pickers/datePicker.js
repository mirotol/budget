import React, { Component } from 'react'
import PropTypes from 'prop-types'
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker
} from '@material-ui/pickers'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
})

class DatePicker extends Component {

    handleDateChange = date => {
        this.props.dateHandler(date)
    }

    render() {
        const { classes } = this.props

        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                    margin='normal'
                    id='date-picker-dialog'
                    label='Date (dd/MM/yyyy)'
                    format='dd/MM/yyyy'
                    value={this.props.date}
                    onChange={this.handleDateChange}
                    KeyboardButtonProps={{
                        'aria-label': 'change date'
                    }}
                />
            </MuiPickersUtilsProvider>
        )
    }
}

DatePicker.propTypes = {
    classes: PropTypes.object.isRequired
}

export default withStyles(styles)(DatePicker)
