import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Store from '../../store.js'
import { connect } from 'react-redux'
import { addExpense } from '../../actions/authActions'
import {
    TextField,
    Input,
    MenuItem,
    Button,
    Dialog,
    DialogTitle,
    FormControl,
    DialogContent,
    DialogActions,
    InputLabel,
    Select
} from '@material-ui/core/'
import 'date-fns'
import { withStyles } from '@material-ui/core/styles'
import DatePicker from '../pickers/datePicker.js'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 300
    },
    button: {
        fontWeight: 500,
        color: theme.palette.secondary.main
    },
    cancelButton: {
        fontWeight: 500
    },
    cssLabel: {
        '&$cssFocused': {
            color: theme.palette.secondary.main
        }
    },
    cssFocused: {},
    cssUnderline: {
        '&:after': {
            borderBottomColor: theme.palette.secondary.main
        }
    }
})

const categories = [
    {
        value: 'Charity',
        label: 'Charity'
    },
    {
        value: 'Clothing',
        label: 'Clothing'
    },
    {
        value: 'Education',
        label: 'Education'
    },
    {
        value: 'Food',
        label: 'Food'
    },
    {
        value: 'Health',
        label: 'Health'
    },
    {
        value: 'Home',
        label: 'Home'
    },
    {
        value: 'Leisure',
        label: 'Leisure'
    },
    {
        value: 'Sports',
        label: 'Sports'
    },
    {
        value: 'Transport',
        label: 'Transport'
    },
    {
        value: 'Other',
        label: 'Other'
    }
]

const repeatCategories = [
    {
        value: 'None',
        label: 'None'
    },
    {
        value: 'Daily',
        label: 'Daily'
    },
    {
        value: 'Weekly',
        label: 'Weekly'
    },
    {
        value: 'Monthly',
        label: 'Monthly'
    }
]

const currentDate = new Date()

class CreateExpense extends Component {
    state = {
        category: '',
        amount: '',
        tag: '',
        repeat: '',
        date: currentDate
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleTextField = name => event => {
        this.setState({ [name]: event.target.value })
    }

    handleDateState = newDate => {
        this.setState({ date: newDate })
    }

    clearStates = () => {
        this.setState({
            category: '',
            tag: '',
            amount: '',
            repeat: '',
            date: currentDate
        })
    }

    handleCancel = () => {
        this.props.dialogHandler()
        this.clearStates()
    }

    handleSubmit = () => {
        const newExpense = {
            expense_category: this.state.category,
            expense_tag: this.state.tag,
            expense_amount: this.state.amount,
            expense_repeat: this.state.repeat,
            expense_date: this.state.date
        }

        const userId = Store.getState().auth.user._id

        this.props.addExpense(userId, newExpense)
        this.clearStates()
        this.props.dialogHandler()
    }

    render() {
        const { classes } = this.props

        return (
            <div>
                <Dialog
                    open={this.props.dialogOpen}
                    onClose={this.props.dialogHandler}
                    aria-labelledby='expense-form-dialog-title'
                >
                    <DialogTitle id='expense-form-dialog-title'>
                        Add expense
                    </DialogTitle>
                    <DialogContent>
                        <form className={classes.container}>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    autoFocus
                                    margin='dense'
                                    id='expense-amount'
                                    label='Amount'
                                    type='number'
                                    value={this.state.amount}
                                    onChange={this.handleTextField('amount')}
                                    fullWidth
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.cssLabel,
                                            focused: classes.cssFocused
                                        }
                                    }}
                                    InputProps={{
                                        classes: {
                                            focused: classes.cssFocused,
                                            underline: classes.cssUnderline
                                        }
                                    }}
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel
                                    htmlFor='category-simple'
                                    classes={{
                                        root: classes.cssLabel,
                                        focused: classes.cssFocused
                                    }}
                                >
                                    Category
                                </InputLabel>
                                <Select
                                    value={this.state.category}
                                    onChange={this.handleChange}
                                    inputProps={{
                                        underline: classes.cssUnderline
                                    }}
                                    input={
                                        <Input
                                            classes={{
                                                underline: classes.cssUnderline
                                            }}
                                            name='category'
                                            id='category-simple'
                                        />
                                    }
                                >
                                    {categories.map(category => (
                                        <MenuItem
                                            key={category.value}
                                            value={category.value}
                                        >
                                            {category.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    margin='dense'
                                    id='expense-tag'
                                    label='Tag'
                                    value={this.state.tag}
                                    onChange={this.handleTextField('tag')}
                                    fullWidth
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.cssLabel,
                                            focused: classes.cssFocused
                                        }
                                    }}
                                    InputProps={{
                                        classes: {
                                            focused: classes.cssFocused,
                                            underline: classes.cssUnderline
                                        }
                                    }}
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel
                                    htmlFor='repeat-category'
                                    classes={{
                                        root: classes.cssLabel,
                                        focused: classes.cssFocused
                                    }}
                                >
                                    Repeat
                                </InputLabel>
                                <Select
                                    value={this.state.repeat}
                                    onChange={this.handleChange}
                                    inputProps={{
                                        underline: classes.cssUnderline
                                    }}
                                    input={
                                        <Input
                                            classes={{
                                                underline: classes.cssUnderline
                                            }}
                                            name='repeat'
                                            id='expense-category'
                                        />
                                    }
                                >
                                    {repeatCategories.map(category => (
                                        <MenuItem
                                            key={category.value}
                                            value={category.value}
                                        >
                                            {category.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <DatePicker
                                    date={this.state.date}
                                    dateHandler={this.handleDateState}
                                />
                            </FormControl>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleCancel}
                            className={classes.button}
                        >
                            Cancel
                        </Button>
                        <Button
                            onClick={this.handleSubmit}
                            className={classes.button}
                        >
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

CreateExpense.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(
    mapStateToProps,
    { addExpense }
)(withStyles(styles, { withTheme: true })(CreateExpense))
