import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import Grid from "@material-ui/core/Grid"
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import ChartistGraph from "react-chartist"
import { GridListTile } from '@material-ui/core'
import GridListContainer from '../grid-list/gridListContainer'
import { VictoryPie, VictoryLabel } from 'victory';

const styles = theme => ({
    root: {
        flexGrow: 1,
        flexWarp: 'wrap',
    },
    title: {
        fontSize: '1.2em',
        fontWeight: 300,
        marginBottom: 16,
        color: theme.palette.text.primary,
    },
    chartStyle: {
        minWidth: 200,
        '& .ct-series-a .ct-line': {
            stroke: '#62a84f',
            'stroke-width': 2,
        },
        '& .ct-series-b .ct-line': {
            stroke: '#a82e2e',
            'stroke-width': 2,
        },
        '& .ct-grid': {
            stroke: '#97743a',
            'stroke-width': 1,
            'stroke-dasharray': 2,
        },
        '& .ct-label': {
            color: '#97743a',
            fontSize: '0.8em',
            fontFamily: 'Roboto',
        },
        '& .ct-series-a .ct-area': {
            fill: '#62a84f',
            fillOpacity: 0.1,
        },
        '& .ct-series-b .ct-area': {
            fill: '#a82e2e',
            fillOpacity: 0.2,
        },
    }
})

class Overview extends Component {
    render() {
        const { classes } = this.props

        var data = {
            labels: ['a', 'b', 'c', 'd'],
            //labels: Array.from({length: 4}, (x,i) => 'W'+ (i+1)),
            series: [
                [40, 20, 21, 5]
                //[200, 50, 1234, 50]
            ]
        }

        var options = {
            donut: true,
            donutWidth: 20,
            donutSolid: true,
            startAngle: 270,
            showLabel: true
        }

        var type = 'Line'

        const data2 = [
            {x: 1, y: 13000},
            {x: 2, y: 16500},
            {x: 3, y: 14250},
            {x: 4, y: 19000}
        ]

        return (
            <GridListContainer>
                <GridListTile cols={1}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography className={classes.title}>
                                Expenses and income
                            </Typography>
                            <ChartistGraph 
                                data={data} 
                                options={options} 
                                type={type} 
                                className={classes.chartStyle}
                            />
                        </CardContent>
                    </Card>
                </GridListTile>
                <GridListTile cols ={4}>
                    <Card className={classes.card}>
                        <CardContent>
                            <Typography className={classes.title}>
                                Expenses and income
                            </Typography>
                            <ChartistGraph 
                                data={data} 
                                options={options} 
                                type={type} 
                                className={classes.chartStyle}
                            />
                        </CardContent>
                    </Card>
                </GridListTile>
            </GridListContainer>
        )
    }
    
}

Overview.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(Overview)
