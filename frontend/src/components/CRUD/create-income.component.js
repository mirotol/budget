import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Store from '../../store.js'
import { connect } from 'react-redux'
import { addIncome } from '../../actions/authActions'
import {
    TextField,
    Input,
    MenuItem,
    Button,
    Dialog,
    DialogTitle,
    FormControl,
    DialogContent,
    DialogActions,
    InputLabel,
    Select
} from '@material-ui/core/'
import 'date-fns'
import { withStyles } from '@material-ui/core/styles'
import DatePicker from '../pickers/datePicker.js'

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column'
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 300
    },
    button: {
        fontWeight: 500,
        color: theme.palette.secondary.main
    },
    cancelButton: {
        fontWeight: 500
    },
    cssLabel: {
        '&$cssFocused': {
            color: theme.palette.secondary.main
        }
    },
    cssFocused: {},
    cssUnderline: {
        '&:after': {
            borderBottomColor: theme.palette.secondary.main
        }
    }
})

const categories = [
    {
        value: 'Salary',
        label: 'Salary'
    },
    {
        value: 'Loan',
        label: 'Loan'
    },
    {
        value: 'Insurance',
        label: 'Insurance'
    },
    {
        value: 'Other',
        label: 'Other'
    }
]

const repeatCategories = [
    {
        value: 'None',
        label: 'None'
    },
    {
        value: 'Daily',
        label: 'Daily'
    },
    {
        value: 'Weekly',
        label: 'Weekly'
    },
    {
        value: 'Monthly',
        label: 'Monthly'
    }
]

const currentDate = new Date()

class CreateIncome extends Component {
    state = {
        category: '',
        amount: '',
        tag: '',
        repeat: '',
        date: currentDate
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleTextField = name => event => {
        this.setState({ [name]: event.target.value })
    }

    handleDateState = newDate => {
        this.setState({ date: newDate })
    }

    clearStates = () => {
        this.setState({
            category: '',
            tag: '',
            amount: '',
            repeat: '',
            date: currentDate
        })
    }

    handleCancel = () => {
        this.props.dialogHandler()
        this.clearStates()
    }

    handleSubmit = () => {
        const newIncome = {
            income_category: this.state.category,
            income_tag: this.state.tag,
            income_amount: this.state.amount,
            income_repeat: this.state.repeat,
            income_date: this.state.date
        }

        const userId = Store.getState().auth.user._id

        this.props.addIncome(userId, newIncome)
        this.clearStates()
        this.props.dialogHandler()
    }

    render() {
        const { classes } = this.props

        return (
            <div>
                <Dialog
                    open={this.props.dialogOpen}
                    onClose={this.props.dialogHandler}
                    aria-labelledby='income-form-dialog-title'
                >
                    <DialogTitle id='income-form-dialog-title'>
                        Add income
                    </DialogTitle>
                    <DialogContent>
                        <form className={classes.container}>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    autoFocus
                                    margin='dense'
                                    id='income-amount'
                                    label='Amount'
                                    type='number'
                                    value={this.state.amount}
                                    onChange={this.handleTextField('amount')}
                                    fullWidth
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.cssLabel,
                                            focused: classes.cssFocused
                                        }
                                    }}
                                    InputProps={{
                                        classes: {
                                            focused: classes.cssFocused,
                                            underline: classes.cssUnderline
                                        }
                                    }}
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel
                                    htmlFor='category-simple'
                                    classes={{
                                        root: classes.cssLabel,
                                        focused: classes.cssFocused
                                    }}
                                >
                                    Category
                                </InputLabel>
                                <Select
                                    value={this.state.category}
                                    onChange={this.handleChange}
                                    inputProps={{
                                        underline: classes.cssUnderline
                                    }}
                                    input={
                                        <Input
                                            classes={{
                                                underline: classes.cssUnderline
                                            }}
                                            name='category'
                                            id='category-simple'
                                        />
                                    }
                                >
                                    {categories.map(category => (
                                        <MenuItem
                                            key={category.value}
                                            value={category.value}
                                        >
                                            {category.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <TextField
                                    margin='dense'
                                    id='income-tag'
                                    label='Tag'
                                    value={this.state.tag}
                                    onChange={this.handleTextField('tag')}
                                    fullWidth
                                    InputLabelProps={{
                                        classes: {
                                            root: classes.cssLabel,
                                            focused: classes.cssFocused
                                        }
                                    }}
                                    InputProps={{
                                        classes: {
                                            focused: classes.cssFocused,
                                            underline: classes.cssUnderline
                                        }
                                    }}
                                />
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <InputLabel
                                    htmlFor='repeat-category'
                                    classes={{
                                        root: classes.cssLabel,
                                        focused: classes.cssFocused
                                    }}
                                >
                                    Repeat
                                </InputLabel>
                                <Select
                                    value={this.state.repeat}
                                    onChange={this.handleChange}
                                    inputProps={{
                                        underline: classes.cssUnderline
                                    }}
                                    input={
                                        <Input
                                            classes={{
                                                underline: classes.cssUnderline
                                            }}
                                            name='repeat'
                                            id='repeat-category'
                                        />
                                    }
                                >
                                    {repeatCategories.map(category => (
                                        <MenuItem
                                            key={category.value}
                                            value={category.value}
                                        >
                                            {category.label}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            <FormControl className={classes.formControl}>
                                <DatePicker
                                    date={this.state.date}
                                    dateHandler={this.handleDateState}
                                />
                            </FormControl>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            onClick={this.handleCancel}
                            className={classes.button}
                        >
                            Cancel
                        </Button>
                        <Button
                            onClick={this.handleSubmit}
                            className={classes.button}
                        >
                            Add
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

CreateIncome.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default connect(
    mapStateToProps,
    { addIncome }
)(withStyles(styles, { withTheme: true })(CreateIncome))
