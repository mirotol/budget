import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import OverviewArea from '../graphs/overview-area'
import OverviewBalance from '../graphs/overview-balanceArea'
import GridListContainer from '../grid-list/gridListContainer'

const styles = theme => ({
    emptyDiv: {
        width: 300
    },
    emptyText: {
        fontWeight: 300
    },
    title: {
        paddingTop: 16,
        paddingLeft: 16,
        fontSize: '1.2em',
        fontWeight: 400,
        color: theme.palette.text.primary
    }
})

class Overview extends Component {
    state = {
        dialogOpen: false
    }

    handleDialogOpen = () => {
        this.setState({ dialogOpen: true })
    }

    handleDialogClose = () => {
        this.setState({ dialogOpen: false })
    }

    render() {
        const { classes } = this.props

        const emptyPage = (
            <div className={classes.emptyDiv}>
                <h2 className={classes.emptyText}>
                    This page is currently empty. Try adding some incomes and
                    expenses.
                </h2>
            </div>
        )

        return (
            <div>
                {this.props.hasIncome ? (
                    <GridListContainer>
                        <OverviewArea />
                        <OverviewBalance />
                    </GridListContainer>
                ) : (
                    emptyPage
                )}
            </div>
        )
    }
}

Overview.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    if (state.auth.user.user_incomes) {
        if (state.auth.user.user_incomes.length > 0) {
            return { hasIncome: true }
        }
    } else {
        return { hasIncome: false }
    }
}

export default connect(mapStateToProps)(withStyles(styles)(Overview))
