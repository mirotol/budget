import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { GridList } from '@material-ui/core'

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.default,
  },
  gridList: {
    minWidth: '82vw',
  }
})

class GridListContainer extends Component {

  render () {
    const { classes, children } = this.props;

    return (
      <div className={classes.root}>
        <GridList  className={classes.gridList} cols={4}>
          {children}
        </GridList>
      </div>
    )
  }
}

GridListContainer.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(GridListContainer);