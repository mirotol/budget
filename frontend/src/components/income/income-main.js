import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core/'
import AddIcon from '@material-ui/icons/AddCircle'
import CreateIncome from '../CRUD/create-income.component'
import GridListContainer from '../grid-list/gridListContainer'
import IncomePie from '../graphs/income-pie'

const styles = theme => ({
    button: {
        whiteSpace: 'nowrap',
        background: '#4caf50',
        '&:hover': {
            background: '#388e3c'
        }
    },
    leftIcon: {
        marginRight: theme.spacing.unit
    },
    emptyDiv: {
        width: 300
    },
    emptyText: {
        fontWeight: 300
    }
})

class IncomeMain extends Component {
    state = {
        dialogOpen: false
    }

    handleDialogOpen = () => {
        this.setState({ dialogOpen: true })
    }

    handleDialogClose = () => {
        this.setState({ dialogOpen: false })
    }

    render() {
        const { classes } = this.props

        const emptyPage = (
            <div className={classes.emptyDiv}>
                <h2 className={classes.emptyText}>
                    This page is currently empty. Try adding some incomes.
                </h2>
            </div>
        )

        return (
            <div>
                <Button
                    variant='contained'
                    color='secondary'
                    className={classes.button}
                    onClick={this.handleDialogOpen}
                >
                    <AddIcon className={classes.leftIcon} />
                    Add income
                </Button>
                <CreateIncome
                    dialogOpen={this.state.dialogOpen}
                    dialogHandler={this.handleDialogClose}
                />
                <br />
                {this.props.hasIncome ? (
                    <GridListContainer>
                        <IncomePie />
                    </GridListContainer>
                ) : (
                    emptyPage
                )}
            </div>
        )
    }
}

IncomeMain.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    if (state.auth.user.user_incomes) {
        if (state.auth.user.user_incomes.length > 0) {
            return { hasIncome: true }
        }
    } else {
        return { hasIncome: false }
    }
}

export default connect(mapStateToProps)(withStyles(styles)(IncomeMain))
