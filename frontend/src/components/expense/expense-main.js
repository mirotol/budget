import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core/'
import AddIcon from '@material-ui/icons/AddCircle'
import CreateExpense from '../CRUD/create-expense.component'
import GridListContainer from '../grid-list/gridListContainer'
import ExpensePie from '../graphs/expense-pie';

const styles = theme => ({
    button: {
        whiteSpace: 'nowrap',
        background: '#4caf50',
        '&:hover': {
            background: '#388e3c'
        }
    },
    leftIcon: {
        marginRight: theme.spacing.unit
    },
    emptyDiv: {
        width: 300
    },
    emptyText: {
        fontWeight: 300
    }
})

class ExpenseMain extends Component {
    state = {
        dialogOpen: false
    }

    handleDialogOpen = () => {
        this.setState({ dialogOpen: true })
    }

    handleDialogClose = () => {
        this.setState({ dialogOpen: false })
    }

    render() {
        const { classes } = this.props

        const emptyPage = (
            <div className={classes.emptyDiv}>
                <h2 className={classes.emptyText}>
                    This page is currently empty. Try adding some expenses.
                </h2>
            </div>
        )

        return (
            <div>
                <Button
                    variant='contained'
                    color='secondary'
                    className={classes.button}
                    onClick={this.handleDialogOpen}
                >
                    <AddIcon className={classes.leftIcon} />
                    Add expense
                </Button>
                <CreateExpense
                    dialogOpen={this.state.dialogOpen}
                    dialogHandler={this.handleDialogClose}
                />
                <br />
                {this.props.hasExpense ? (
                    <GridListContainer>
                        <ExpensePie />
                    </GridListContainer>
                ) : (
                    emptyPage
                )}
            </div>
        )
    }
}

ExpenseMain.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    if (state.auth.user.user_expenses) {
        if (state.auth.user.user_expenses.length > 0) {
            return { hasExpense: true }
        }
    } else {
        return { hasExpense: false }
    }
}

export default connect(mapStateToProps)(withStyles(styles)(ExpenseMain))
