import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    BrowserRouter as Router,
    Route,
    Link,
    withRouter
} from 'react-router-dom'
import {
    AppBar,
    Drawer,
    Divider,
    MenuList,
    MenuItem,
    List,
    ListItem,
    ListItemText,
    Hidden,
    IconButton,
    Toolbar,
    Typography
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { logoutUser } from '../../actions/authActions'

const drawerWidth = 220

const styles = theme => ({
    root: {
        width: drawerWidth,
        display: 'flex'
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1
    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0
        }
    },
    drawerPaper: {
        background: theme.palette.primary.main,
        width: drawerWidth
        //backgroundImage: 'url(' + DrawerImage + ')'
    },
    divider: {
        background: '#8a8a8a'
    },
    listText: {
        fontWeight: 300,
        color: theme.palette.text.contrastText
    },
    menuButton: {
        color: theme.palette.secondary.main,
        marginRight: 20,
        [theme.breakpoints.up('md')]: {
            display: 'none'
        }
    },
    menuItem: {
        fontWeight: 300,
        color: theme.palette.text.contrastText,
        '&:focus': {
            backgroundColor: theme.palette.secondary.dark,
            '& $primary, & $icon': {
                color: theme.palette.common.white
            }
        }
    },
    primary: {},
    icon: {},
    content: {
        flexGrow: 1,
        padding: theme.spacing.unit * 2
    },
    toolbar: theme.mixins.toolbar,
    toolbarText: {
        color: theme.palette.text.contrastText
    }
})

const routes = [
    {
        path: '/',
        component: Link,
        text: 'Overview'
    },
    {
        path: '/income',
        component: Link,
        text: 'Incomes'
    },
    {
        path: '/expense',
        component: Link,
        text: 'Expenses'
    },
    {
        path: '/profile',
        component: Link,
        text: 'Profile'
    }
]

class Sidebar extends Component {
    state = {
        mobileOpen: false,
        appBarTitle: 'Overview'
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }))
    }

    handleAppBarTitle = title => {
        this.setState(state => ({ appBarTitle: title }))
    }

    onLogout = e => {
        e.preventDefault()
        this.props.logoutUser(this.props.history)
    }

    render() {
        const { classes, children } = this.props

        const drawer = (
            <div>
                <MenuList>
                    {routes.map(route => (
                        <MenuItem
                            selected={false}
                            className={classes.menuItem}
                            key={route.text}
                            component={route.component}
                            to={route.path}
                            onClick={() => this.handleAppBarTitle(route.text)}
                        >
                            <ListItemText
                                classes={{ primary: classes.menuItem }}
                                primary={route.text}
                            />
                        </MenuItem>
                    ))}
                    <Divider className={classes.divider} />
                    <MenuItem
                        selected={false}
                        className={classes.menuItem}
                        key={'Logout'}
                        onClick={this.onLogout}
                    >
                        <ListItemText
                            classes={{ primary: classes.menuItem }}
                            primary={'Logout'}
                        />
                    </MenuItem>
                </MenuList>
            </div>
        )

        return (
            <div className={classes.root}>
                <Hidden mdUp>
                    <AppBar
                        position='fixed'
                        className={classes.appBar}
                        color='primary'
                    >
                        <Toolbar>
                            <IconButton
                                aria-label='Open drawer'
                                onClick={this.handleDrawerToggle}
                                className={classes.menuButton}
                            >
                                <MenuIcon />
                            </IconButton>
                            <Typography
                                variant='h6'
                                noWrap
                                className={classes.toolbarText}
                            >
                                {this.state.appBarTitle}
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </Hidden>
                <Hidden mdUp>
                    <Drawer
                        container={this.props.container}
                        variant='temporary'
                        anchor='left'
                        open={this.state.mobileOpen}
                        onClose={this.handleDrawerToggle}
                        classes={{
                            paper: classes.drawerPaper
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden smDown>
                    <Drawer
                        className={classes.drawer}
                        variant='permanent'
                        classes={{
                            paper: classes.drawerPaper
                        }}
                        anchor='left'
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <main className={classes.content}>
                    <Hidden mdUp>
                        <div className={classes.toolbar} />
                    </Hidden>
                    {children}
                </main>
            </div>
        )
    }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
})

export default withRouter(
    connect(
        mapStateToProps,
        { logoutUser }
    )(withStyles(styles)(Sidebar))
)
