import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { GridListTile, Card, CardContent, Typography } from '@material-ui/core/'
import { VictoryLabel, VictoryPie } from 'victory'

const styles = theme => ({
    title: {
        paddingTop: 16,
        paddingLeft: 16,
        fontSize: '1.2em',
        fontWeight: 400,
        color: theme.palette.text.primary
    },
    card: {
        minWidth: 500,
        minHeight: 500,
        marginRight: 15,
        marginBottom: 15
    },
    svg: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    cardContent: {
        padding: 4
    }
})

class ExpensePie extends Component {
    render() {
        const { classes } = this.props

        const orangeColorScale = [
            '#b37400',
            '#cc8400',
            '#e69500',
            '#ffa500',
            '#ffae1a',
            '#ffb733',
            '#ffc04d'
        ]

        return (
            <GridListTile>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography className={classes.title}>
                            Total expense
                        </Typography>
                        <svg viewBox='-50 -50 500 500'>
                            <VictoryPie
                                padAngle={2}
                                colorScale={orangeColorScale}
                                data={this.props.userExpense}
                                standalone={false}
                                innerRadius={110}
                                labelRadius={170}
                                labelComponent={<VictoryLabel angle={0} />}
                                style={{
                                    labels: {
                                        fontSize: 20,
                                        fontWeight: 400,
                                        fill: 'rgba(0, 0, 0, 0.87)',
                                        fontFamily: 'Roboto'
                                    }
                                }}
                            />
                            <VictoryLabel
                                textAnchor='middle'
                                style={{
                                    fontSize: 22,
                                    fontWeight: 400,
                                    fill: 'rgba(0, 0, 0, 0.87)',
                                    fontFamily: 'Roboto'
                                }}
                                text={this.props.totalExpense}
                                x={200}
                                y={200}
                            />
                        </svg>
                    </CardContent>
                </Card>
            </GridListTile>
        )
    }
}

ExpensePie.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    // Map income data for chart
    if (state.auth.user.user_expenses) {
        const data = state.auth.user.user_expenses.map(o => {
            const chartData = { x: '', y: '' }
            chartData.x = o.expense_category
            chartData.y = o.expense_amount
            return chartData
        })

        // Sort data by income amount
        data.sort((a, b) => parseFloat(a.y) - parseFloat(b.y))

        const totalExpense = () => {
            let x = 0
            data.forEach(element => {
                x += parseFloat(element.y)
            })
            return x + ' €'
        }
        return {
            userExpense: data,
            totalExpense: totalExpense,
            errors: state.errors
        }
    } else {
        return {}
    }
}

export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(ExpensePie)
)
