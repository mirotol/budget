import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { GridListTile, Card, CardContent, Typography } from '@material-ui/core/'
import {
    VictoryAxis,
    VictoryArea,
    VictoryChart,
    VictoryGroup,
    VictoryTheme
} from 'victory'
import {
    sortIncomeByDate,
    sortExpenseByDate,
    sumPerDay,
    lastThirtyExpense,
    lastThirtyIncome
} from '../../helperFunctions/overviewHelper'
import { format } from 'date-fns'

const styles = theme => ({
    title: {
        paddingTop: 16,
        paddingLeft: 16,
        fontSize: '1.2em',
        fontWeight: 400,
        color: theme.palette.text.primary
    },
    card: {
        minWidth: 800,
        minHeight: 500,
        marginRight: 15,
        marginBottom: 15
    },
    cardContent: {
        padding: 4
    }
})

class OverviewArea extends Component {
    render() {
        const { classes } = this.props

        return (
            <GridListTile>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography className={classes.title}>
                            Last 30 days income and expense
                        </Typography>
                        <VictoryChart width={800} height={500}>
                            <VictoryGroup
                                animate={{
                                    duration: 1500,
                                    onLoad: {
                                        duration: 200
                                    }
                                }}
                                style={{
                                    data: { strokeWidth: 2, fillOpacity: 0.8 },
                                    grid: {
                                        stroke: 'red'
                                    }
                                }}
                            >
                                <VictoryArea
                                    data={this.props.incomePerDay}
                                    interpolation='linear'
                                    style={{
                                        data: {
                                            fill: '#006600',
                                            stroke: '#006600'
                                        }
                                    }}
                                />
                                <VictoryArea
                                    data={this.props.expensePerDay}
                                    interpolation='linear'
                                    style={{
                                        data: {
                                            fill: '#990033',
                                            stroke: '#990033'
                                        }
                                    }}
                                />
                            </VictoryGroup>
                        </VictoryChart>
                    </CardContent>
                </Card>
            </GridListTile>
        )
    }
}

OverviewArea.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    // Map income data for chart
    if (state.auth.user.user_incomes) {
        const incomeDateArray = sortIncomeByDate(state.auth.user.user_incomes)
        const incomeFiltered = lastThirtyIncome(incomeDateArray)

        const monthlyIncome = incomeFiltered.map(o => {
            const chartData = { x: '', y: '' }
            const date = new Date(o.income_date).getTime()
            chartData.x = new Date(format(date, 'MMM d'))
            chartData.y = Number(o.income_amount)
            return chartData
        })

        const income = sumPerDay(monthlyIncome)

        const expenseDateArray = sortExpenseByDate(
            state.auth.user.user_expenses
        )
        const expenseFiltered = lastThirtyExpense(expenseDateArray)

        const monthlyExpense = expenseFiltered.map(o => {
            const chartData = { x: '', y: '' }
            const date = new Date(o.expense_date).getTime()
            chartData.x = new Date(format(date, 'MMM d'))
            chartData.y = Number(o.expense_amount)
            return chartData
        })

        const expense = sumPerDay(monthlyExpense)

        return {
            incomePerDay: income,
            expensePerDay: expense,
            errors: state.errors
        }
    } else {
        return {}
    }
}

export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(OverviewArea)
)
