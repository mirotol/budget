import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { GridListTile, Card, CardContent, Typography } from '@material-ui/core/'
import { VictoryLabel, VictoryPie } from 'victory'

const styles = theme => ({
    title: {
        paddingTop: 16,
        paddingLeft: 16,
        fontSize: '1.2em',
        fontWeight: 400,
        color: theme.palette.text.primary
    },
    card: {
        minWidth: 500,
        minHeight: 500,
        marginRight: 15,
        marginBottom: 15
    },
    svg: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    cardContent: {
        padding: 4
    }
})

class IncomePie extends Component {
    render() {
        const { classes } = this.props

        const greenColorScale = [
            '#244a15',
            '#33691e',
            '#428726',
            '#51a62f',
            '#60c538',
            '#d6bb19',
            '#e8cf3b'
        ]

        return (
            <GridListTile>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography className={classes.title}>
                            Total income
                        </Typography>
                        <svg viewBox='-50 -50 500 500'>
                            <VictoryPie
                                padAngle={2}
                                colorScale={greenColorScale}
                                data={this.props.userIncome}
                                standalone={false}
                                innerRadius={110}
                                labelRadius={170}
                                labelComponent={<VictoryLabel angle={0} />}
                                style={{
                                    labels: {
                                        fontSize: 20,
                                        fontWeight: 400,
                                        fill: 'rgba(0, 0, 0, 0.87)',
                                        fontFamily: 'Roboto'
                                    }
                                }}
                            />
                            <VictoryLabel
                                textAnchor='middle'
                                style={{
                                    fontSize: 22,
                                    fontWeight: 400,
                                    fill: 'rgba(0, 0, 0, 0.87)',
                                    fontFamily: 'Roboto'
                                }}
                                text={this.props.totalIncome}
                                x={200}
                                y={200}
                            />
                        </svg>
                    </CardContent>
                </Card>
            </GridListTile>
        )
    }
}

IncomePie.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    // Map income data for chart
    if (state.auth.user.user_incomes) {
        const data = state.auth.user.user_incomes.map(o => {
            const chartData = { x: '', y: '' }
            chartData.x = o.income_category
            chartData.y = o.income_amount
            return chartData
        })

        // Sort data by income amount
        data.sort((a, b) => parseFloat(a.y) - parseFloat(b.y))

        const totalIncome = () => {
            let x = 0
            data.forEach(element => {
                x += parseFloat(element.y)
            })
            return x + ' €'
        }
        return {
            userIncome: data,
            totalIncome: totalIncome,
            errors: state.errors
        }
    } else {
        return {}
    }
}

export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(IncomePie)
)
