import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { GridListTile, Card, CardContent, Typography } from '@material-ui/core/'
import {
    VictoryArea,
    VictoryAxis,
    VictoryChart,
    VictoryLine,
    VictoryGroup,
    VictoryScatter,
    VictoryTooltip,
    VictoryVoronoiContainer
} from 'victory'
import {
    calculateBalance,
    sortIncomeByDate,
    sortExpenseByDate,
    sumPerDay,
    lastThirtyExpense,
    lastThirtyIncome
} from '../../helperFunctions/overviewHelper'
import { format } from 'date-fns'

const styles = theme => ({
    title: {
        paddingTop: 16,
        paddingLeft: 16,
        fontSize: '1.2em',
        fontWeight: 400,
        color: theme.palette.text.primary
    },
    card: {
        minWidth: 800,
        minHeight: 500,
        marginRight: 15,
        marginBottom: 15
    },
    cardContent: {
        padding: 4
    }
})

class OverviewBalanceArea extends Component {
    render() {
        const { classes } = this.props

        return (
            <GridListTile>
                <Card className={classes.card}>
                    <CardContent className={classes.cardContent}>
                        <Typography className={classes.title}>
                            Last 30 days balance
                        </Typography>
                        <VictoryChart
                            width={800}
                            height={500}
                            padding={{
                                left: 100,
                                right: 50,
                                top: 50,
                                bottom: 50
                            }}
                            domainPadding={{ y: 50 }}
                            containerComponent={
                                <VictoryVoronoiContainer
                                    labels={d =>
                                        d.y + ' (' + format(d.x, 'MMM d') + ')'
                                    }
                                    labelComponent={
                                        <VictoryTooltip
                                            style={{
                                                fontFamily: 'roboto'
                                            }}
                                            cornerRadius={0}
                                            flyoutStyle={{ fill: 'white' }}
                                        />
                                    }
                                />
                            }
                        >
                            <VictoryLine
                                data={this.props.balance}
                                interpolation='linear'
                                scale={{ x: 'time', y: 'linear' }}
                                style={{
                                    data: {
                                        stroke: '#6600cc',
                                        strokeWidth: 4
                                    }
                                }}
                            />
                            <VictoryAxis
                                data={this.props.balance}
                                style={{
                                    ticks: {
                                        size: 10,
                                        stroke: 'black',
                                        strokeOpacity: 1
                                    },
                                    tickLabels: {
                                        fontFamily: 'Roboto',
                                        fontSize: 18
                                    }
                                }}
                            />
                            <VictoryAxis
                                dependentAxis
                                data={this.props.balance}
                                style={{
                                    ticks: {
                                        size: 10,
                                        stroke: 'black',
                                        strokeOpacity: 1
                                    },
                                    tickLabels: {
                                        fontFamily: 'Roboto',
                                        fontSize: 18
                                    }
                                }}
                            />
                        </VictoryChart>
                    </CardContent>
                </Card>
            </GridListTile>
        )
    }
}

OverviewBalanceArea.propTypes = {
    classes: PropTypes.object.isRequired
}

const mapStateToProps = state => {
    // Map income data for chart
    if (state.auth.user.user_incomes) {
        const incomeDateArray = sortIncomeByDate(state.auth.user.user_incomes)
        const incomeFiltered = lastThirtyIncome(incomeDateArray)

        const monthlyIncome = incomeFiltered.map(o => {
            const chartData = { x: '', y: '' }
            const date = new Date(o.income_date).getTime()
            chartData.x = new Date(format(date, 'MMM d'))
            chartData.y = Number(o.income_amount)
            return chartData
        })

        const income = sumPerDay(monthlyIncome)

        const expenseDateArray = sortExpenseByDate(
            state.auth.user.user_expenses
        )
        const expenseFiltered = lastThirtyExpense(expenseDateArray)

        const monthlyExpense = expenseFiltered.map(o => {
            const chartData = { x: '', y: '' }
            const date = new Date(o.expense_date).getTime()
            chartData.x = new Date(format(date, 'MMM d'))
            chartData.y = Number(o.expense_amount)
            console.log(chartData)
            return chartData
        })

        const expense = sumPerDay(monthlyExpense)

        const balance = calculateBalance(income, expense, 0)
        console.log(balance)
        return {
            balance: balance,
            errors: state.errors
        }
    } else {
        return {}
    }
}

export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(OverviewBalanceArea)
)
