import { SET_CURRENT_USER, USER_LOADING, ADD_INCOME, ADD_EXPENSE } from '../actions/types'

const isEmpty = require('is-empty')
const initialState = {
    isAuthenticated: false,
    user: {},
    loading: false
}

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticated: !isEmpty(action.payload),
                user: action.payload
            }
        case USER_LOADING:
            return {
                ...state,
                loading: true
            }
        case ADD_INCOME:
            return {
                ...state,
                user: {
                    ...state.user,
                    user_incomes: action.payload
                }
            }
        case ADD_EXPENSE:
            return {
                ...state,
                user: {
                    ...state.user,
                    user_expenses: action.payload
                }
            }
        default:
            return state
    }
}
