import {
    isThisMonth,
    isWithinInterval,
    isEqual,
    eachDayOfInterval,
    subDays
} from 'date-fns'
import { format } from 'date-fns/esm'

export const sortIncomeByDate = array => {
    array.sort((a, b) => {
        return new Date(a.income_date) - new Date(b.income_date)
    })
    return array
}

export const sortExpenseByDate = array => {
    array.sort((a, b) => {
        return new Date(a.expense_date) - new Date(b.expense_date)
    })
    return array
}

export const filterIncomeByCurrentMonth = array => {
    let filteredArray = []
    array.forEach(o => {
        if (isThisMonth(new Date(o.income_date))) {
            filteredArray.push(o)
        }
    })
    return filteredArray
}

export const filterExpenseByCurrentMonth = array => {
    let filteredArray = []
    array.forEach(o => {
        if (isThisMonth(new Date(o.expense_date))) {
            filteredArray.push(o)
        }
    })
    return filteredArray
}

export const lastThirtyIncome = array => {
    let filteredArray = []
    array.forEach(o => {
        if (
            isWithinInterval(new Date(o.income_date), {
                start: subDays(new Date(), 29),
                end: new Date()
            })
        ) {
            filteredArray.push(o)
        }
    })
    return filteredArray
}

export const lastThirtyExpense = array => {
    let filteredArray = []
    array.forEach(o => {
        if (
            isWithinInterval(new Date(o.expense_date), {
                start: subDays(new Date(), 29),
                end: new Date()
            })
        ) {
            filteredArray.push(o)
        }
    })
    return filteredArray
}

export const sumPerDay = array => {
    let sum = []

    array.forEach(o => {
        let existing = sum.filter(i => {
            return isEqual(o.x, i.x)
        })[0]
        if (!existing) {
            sum.push(o)
        } else {
            existing.y += o.y
        }
    })

    return sum
}

export const labelsForThirty = () => {
    const result = eachDayOfInterval({
        start: subDays(new Date(), 29),
        end: new Date()
    })
    const formattedResult = result.map(date => new Date(format(date, 'MMM d')))

    return formattedResult
}

export const calculateBalance = (incomeArray, expenseArray, startBalance) => {
    let balanceSum = startBalance
    const dates = labelsForThirty()
    const balance = dates.map(o => {
        const balanceObject = { x: '', y: '' }
        balanceObject.x = o
        incomeArray.forEach(income => {

            if (isEqual(balanceObject.x, income.x)) {
                balanceSum += income.y
            }
        })
        expenseArray.forEach(expense => {
            if (isEqual(balanceObject.x, expense.x)) {
                balanceSum -= expense.y
            }
        })
        balanceObject.y = balanceSum
        return balanceObject
    })

    return balance
}