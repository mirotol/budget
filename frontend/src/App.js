import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'
import { getSession } from './actions/authActions'
import Store from './store'

import 'typeface-roboto'

import Overview from './components/overview/overview-main'
import Layout from './components/layout/layout'
import IncomeMain from './components/income/income-main'
import ExpenseMain from './components/expense/expense-main'
import Register from './components/login/register'
import Login from './components/login/login'

getSession()

class App extends Component {
    
    render() {

        return (
            <Provider store={Store}>
                <Router>
                    <Switch>
                        <Route exact path='/register' component={Register} />
                        <Route exact path='/login' component={Login} />
                        <Layout>
                            <Route exact path='/' component={Overview} />
                            <Route exact path='/income' component={IncomeMain} />
                            <Route exact path='/expense' component={ExpenseMain} />
                        </Layout>
                    </Switch>
                </Router>
            </Provider>
        )
    }
}

export default App
