import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core'
import CssBaseline from '@material-ui/core/CssBaseline';

const theme = createMuiTheme({
    palette: {
        action: {
            selected: '#8c0032',
            hover: 'rgba(138, 0, 50, 0.5)', // #8a0030 with 0.5 opacity
        },
        background: {
            default: '#E1E2E1',
            paper: '#f5f5f6',
        },
        primary: {
            main: '#1b1b1b',
        },
        secondary: {
            main: '#c2185b',
            dark: '#8c0032'
        },
        text: {
            primary: 'rgba(0, 0, 0, 0.87)',
            secondary: 'rgba(0, 0, 0, 0.65)',
            contrastText: '#e0e0e0'
        }
    },
    typography: {
        body1: {
            fontWeight: 300,
        },
        button: {
            fontWeight: 400,
        },
        h6: {
            fontWeight: 300,
        },
        title: {
            fontWeight: 400,
        }
    },
})

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <App /> 
    </MuiThemeProvider>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
