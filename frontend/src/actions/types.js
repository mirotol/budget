// Login
export const GET_ERRORS = 'GET_ERRORS'
export const USER_LOADING = 'USER_LOADING'
export const SET_CURRENT_USER = 'SET_CURRENT_USER'

// Income
export const ADD_INCOME = 'ADD_INCOME'

// Expense
export const ADD_EXPENSE = 'ADD_EXPENSE'