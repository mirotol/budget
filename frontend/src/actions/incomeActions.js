import axios from 'axios'
import { GET_ERRORS, ADD_INCOME } from './types'

export const addIncome = (userId, newIncome) => dispatch => {
    axios
        .post('/user/' +userId+ '/income/add', newIncome)
        .then(res => console.log(res)) // re-direct to login on successful register
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        )
}