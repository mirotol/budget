import axios from 'axios'
import { GET_ERRORS, SET_CURRENT_USER, USER_LOADING, ADD_INCOME, ADD_EXPENSE } from './types'
import Store from '../store'

// Register User
export const registerUser = (userData, history) => dispatch => {
    axios
        .post('/user/add', userData)
        .then(res => history.push(res.data.redirect)) // re-direct to login on successful register
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        )
}
// Login - get user token
export const loginUser = userData => dispatch => {
    axios
        .post('/auth/login', userData)
        .then(res => {
            // Set current user
            dispatch(setCurrentUser(res.data.userData))
        })
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        )
}
// Set logged in user
export const setCurrentUser = decoded => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
}
// User loading
export const setUserLoading = () => {
    return {
        type: USER_LOADING
    }
}
// Log user out
export const logoutUser = history => dispatch => {
    // Set current user to empty object {} which will set isAuthenticated to false
    axios
        .post('/auth/logout')
        .then(res => {
            dispatch(setCurrentUser({}))
            history.push(res.data.redirect)
        })
}

export const getSession = dispatch => {
    if (!Store.getState().auth.isAuthenticated) {
        axios
            .get('/auth/session')
            .then(res => {
                if (res.data) {
                    Store.dispatch(setCurrentUser(res.data.userData))
                }
            })
            .catch(err => {
                Store.dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                })
            }
                
            )
    }
}

export const addIncome = (userId, newIncome) => dispatch => {
    axios
        .post('/user/' + userId + '/income/add', newIncome)
        .then(res =>
            dispatch({
                type: ADD_INCOME,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        )
}

export const addExpense = (userId, newExpense) => dispatch => {
    axios
        .post('/user/' + userId + '/expense/add', newExpense)
        .then(res =>
            dispatch({
                type: ADD_EXPENSE,
                payload: res.data
            })
        )
        .catch(err =>
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            })
        )
}