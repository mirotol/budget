const LocalStrategy = require('passport-local').Strategy
//const bcrypt = require('bcryptjs')

const User = require('../models/user.model')

module.exports = function(passport) {
    passport.use(
        new LocalStrategy(
            { usernameField: 'email' },
            (email, password, done) => {
                User.findOne({ user_email: email })
                    .then(user => {
                        if (!user) {
                            return done(null, false)
                        }
                        //bcrypt.compare(password, user.password)
                        if (password === user.user_password) {
                            return done(null, user)
                        } else {
                            return done(null, false)
                        }
                    })
                    .catch(err => console.log(err))
            }
        )
    )
    passport.serializeUser((user, done) => {
        done(null, user.id)
    })

    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user)
        })
    })
}
