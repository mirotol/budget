const express = require('express')
const app = express()
const bodyParser = require('body-parser')
//const cors = require('cors')
const mongoose = require('mongoose')
const passport = require('passport')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)

const PORT = 4000

const user = require('./routes/user')
const auth = require('./routes/auth')
//const income = require('./routes/income')
//const expense = require('./routes/expense')

// Passport config
require('./config/passport')(passport)

mongoose.connect('mongodb://127.0.0.1:27017/budget', { useNewUrlParser: true })
const connection = mongoose.connection

connection.once('open', function() {
    console.log('MongoDB database connection established successfully')
})

//app.use(cors())
app.use(bodyParser.json())

// Express session
app.use(
    session({
        secret: 'randomGenerate', // String should be random generated later...
        store: new MongoStore({
            mongooseConnection: connection,
            ttl: 24 * 60 * 60 // 24h expire time
        }),
        resave: false, // Update session even if no changes were made
        saveUninitialized: false, // Create cookie even if user didn't log in
    })
)

// Passport middleware
app.use(passport.initialize())
app.use(passport.session())

// Routes
app.use('/user', user)
app.use('/auth', auth)
//app.use('/income', income)
//app.use('/expense', expense)

app.listen(PORT, function() {
    console.log('Server is running on Port: ' + PORT)
})
