const express = require('express')
const router = express.Router()
const passport = require('passport')

const User = require('../models/user.model.js')
const Income = require('../models/income.model.js')
const Expense = require('../models/expense.model.js')

router.route('/').get((req, res) => {
    User.find(function(err, user) {
        if (err) {
            console.log(err)
        } else {
            res.json(user)
        }
    })
})

router.route('/:id').get(function(req, res) {
    let id = req.params.id
    User.findById(id, function(err, user) {
        res.json(user)
    })
})

/*router.route('/email/:email').get(function(req, res) {
    let email = req.params.email
    User.findOne({ user_email: email })
        .then(user => {
            res.json(user)
        })
        .catch(err => {
            console.log(err)
        })
})*/

router.route('/add').post(function(req, res) {
    let newUser = new User(req.body)
    User.findOne({ user_email: newUser.user_email }).then(user => {
        if (user) {
            res.json({
                user: 'Email is already registered',
                redirect: '/register'
            })
        } else {
            newUser
                .save()
                .then(newUser => {
                    res.status(200).json({
                        user: 'user added succesfully',
                        redirect: '/login'
                    })
                })
                .catch(err => {
                    res.status(400).send('adding new user failed')
                })
        }
    })
})

router.route('/:id').delete(function(req, res) {
    let id = req.params.id
    User.findByIdAndDelete(id, function(err, user) {
        res.json({ user: 'user deleted by id' })
    })
})

router.route('/:id/income/add').post((req, res) => {
    let id = req.params.id
    let newIncome = new Income(req.body)
    User.findById(id, function(err, user) {
        user.user_incomes.push(newIncome)
        user.save()
            .then(user => {
                const incomeArray = Array.from(user.user_incomes) // Convert CoreMongooseArray to Array
                res.json(incomeArray)
            })
            .catch(err => {
                res.status(400).send('Adding new income not possible')
            })
    })
})

router.route('/:id/expense/add').post((req, res) => {
    let id = req.params.id
    let newExpense = new Expense(req.body)
    User.findById(id, function(err, user) {
        user.user_expenses.push(newExpense)
        user.save()
            .then(user => {
                const expenseArray = Array.from(user.user_expenses) // Convert CoreMongooseArray to Array
                res.json(expenseArray)
            })
            .catch(err => {
                res.status(400).send('Adding new expense not possible')
            })
    })
})
module.exports = router
