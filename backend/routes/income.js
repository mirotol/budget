const express = require('express')
const router = express.Router()

const Income = require('../models/income.model.js')

router.route('/').get(function(req, res) {
    Income.find(function(err, income) {
        if (err) {
            console.log(err)
        } else {
            res.json(income)
        }
    })
})

router.route('/:id').get(function(req, res) {
    let id = req.params.id
    Income.findById(id, function(err, income) {
        res.json(income)
    })
})

router.route('/add').post(function(req, res) {
    let income = new Income(req.body)
    income.save()
        .then(income => {
            res.status(200).json({
                'income': 'income added succesfully'
            })
        })
        .catch(err => {
            res.status(400).send('adding new income failed')
        })
})

router.route('/update/:id').post(function(req, res) {
    Income.findById(req.params.id, function(err, income) {
        if (!income)
            res.status(404).send("data is not found")
        else
            income.income_category = req.body.income_category
            income.income_tag = req.body.income_tag
            income.income_amount = req.body.income_amount

            income.save().then(income => {
                res.json('Income updated!')
            })
            .catch(err => {
                res.status(400).send("Update not possible")
            })
    })
})

router.route('/:id').delete(function(req, res) {
    let id = req.params.id
    Income.findByIdAndDelete(id, function(err, income) {
        res.json({'income': 'income deleted by id'})
    })
})

module.exports = router