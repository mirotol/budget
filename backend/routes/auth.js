const express = require('express')
const router = express.Router()
const passport = require('passport')
const User = require('../models/user.model.js')

// Login handle
router.route('/login').post((req, res, next) => {
    // Authenticate function is defined in passport.js file
    passport.authenticate(
        'local',
        (err, user, info) => {
            if (err) {
                return next(err)
            }
            if (!user) {
                return res
                    .status(404)
                    .json({ emailnotfound: 'Email not found' })
            }
            req.logIn(user, err => {
                if (err) {
                    return next(err)
                }
                let userData = {
                    _id: user._id,
                    user_name: user.user_name,
                    user_email: user.user_email,
                    user_incomes: user.user_incomes,
                    user_expenses: user.user_expenses
                }
                return res.json({ userData })
            })
        }
        //successRedirect: '/',
        //failureRedirect: '/user/login'
    )(req, res, next)
})

// Logout handle
router.route('/logout').post((req, res) => {
    req.logOut()
    res.json({ redirect: '/login' })
})

// Session handle
router.route('/session').get((req, res, next) => {
    const userId = req.session.passport.user
    if (userId) {
        User.findById(userId, (err, user) => {
            if (err) {
                return next(err)
            }
            if (!user) {
                return res
                    .status(404)
                    .json({ userNotFound: 'User was not found' })
            }
            req.logIn(user, err => {
                if (err) {
                    return next(err)
                }
                let userData = {
                    _id: user._id,
                    user_name: user.user_name,
                    user_email: user.user_email,
                    user_incomes: user.user_incomes,
                    user_expenses: user.user_expenses
                }
                return res.json({ userData })
            })
        })
    }
})

module.exports = router
