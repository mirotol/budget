const express = require('express')
const router = express.Router()

const Expense = require('../models/expense.model.js')

router.route('/').get(function(req, res) {
    Expense.find(function(err, expense) {
        if (err) {
            console.log(err)
        } else {
            res.json(expense)
        }
    })
})

module.exports = router