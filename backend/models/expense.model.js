const mongoose = require('mongoose')
const Schema = mongoose.Schema

let Expense = new Schema({
    expense_category: {
        type: String
    },
    expense_tag: {
        type: String
    },
    expense_amount: {
        type: String
    },
    expense_repeat: {
        type: String
    },
    expense_date: {
        type: String
    },
    expense_createdAt: {
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('Expense', Expense)