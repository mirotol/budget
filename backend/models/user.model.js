const mongoose = require("mongoose")
const Schema = mongoose.Schema
const Income = require("./income.model.js")
const Expense = require("./expense.model.js")

let User = new Schema({
    user_name: {
        type: String,
        required: true
    },
    user_email: {
        type: String,
        required: true
    },
    user_password: {
        type: String,
        required: true
    },
    user_createdAt: {
        type: Date,
        default: Date.now
    },
    user_incomes: [Income.schema],
    user_expenses: [Expense.schema]
})

module.exports = mongoose.model("User", User)
