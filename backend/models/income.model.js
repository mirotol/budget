const mongoose = require('mongoose')
const Schema = mongoose.Schema

let Income = new Schema({
    income_category: {
        type: String
    },
    income_tag: {
        type: String
    },
    income_amount: {
        type: String
    },
    income_repeat: {
        type: String
    },
    income_date: {
        type: String
    },
    income_createdAt: {
        type: Date,
        default: Date.now
    },
})

module.exports = mongoose.model('Income', Income)